# Egyedi, geometriák alapján lekérdezést végrehajtó modul

A modul célja nagy méretű postgis táblában keresni, és az abban található geometriák alapján lekérdezéseket végehajtani egy adott projekttáblában.
Jelenleg a csomagban két önálló lekérdező modul van előkészítve. A hrsz_query helyrajzi számok alapján hajtja végre a lekérdezést, míg az erdo_query erdőrészletek alapján kérdezi le az adatokat. Természetesen ezek alapján bárki készíthet magának saját egyedi lekérdezéseket pl. UTM kvadrátra, ETRS háló elemeire, stb.

A modul egyedileg paraméterezhető, így bárki elkészíthet magának hasonló modulokat az OBM térképi nézetéhez.

A modulhoz engedélyezni kell a box_custom modult, ahol a paramétereknél kell definiálni az elkészített modul(oka)t. Bővebben a box_custom modulról az OBM dokumentációban lehet olvasni, az alábbi linken:

[Modulok](http://openbiomaps.org/documents/hu/modules.html)

## Előfeltételek

### Tábla létrehozása

A create_table.sql scriptek segítségével a fent említett két önálló modulhoz lehet létrehozni a táblákat. Ezek a működéshez feltétlenül szükséges minimális adattartalommal jönnek létre, amelybe a szükséges geometriák, attribútumok bemásolhatók. Kiegészíthető egyéb szükséges oszlopokkal is, azonban a jelenlegi beállításokkal azok a lekérdezésben semmilyen szerepet nem fognak betölteni a modul működése során.
A tábla létrehozása történhet úgy is, hogy a gis állományunkat importáljuk a postgis adatbázisba pl. QGIS segítségével. Ebben az esetben használat előtt a modulban és a hozzá kapcsolódó sql parancsokban a tábla és mezőneveket, címet aktualizálni kell. A kódban az érték felett egy kommenttel jelöltük, ahol a változtatásokat esetleg meg kell tenni. Az alábbi értékek változtatására lehet szükség:

* osztálynév (csak teljesen új lekérdezés készítése esetén (pl. UTM kvadrát, stb.)
* sémanév.táblanév
* modul megjelenített címe (Ez jelenik meg a weboldalon a felhasunáló számára.)
* lekérdező oszlop (pl. településnév)
* szűrő oszlop (pl. hrsz, mindenképpen a szűrést követően egyedi érték.)

### Definiálás projekttáblaként

Azt a táblát, ami alapján majd a leklérdezéseket hajtjuk végre, hozzá kell adni az adott projekt projekt tábláihoz.
Ehhez a biomaps adatbázis public.header_names táblájába kell beszúrni egy sort, amelyben legalább az alábbi mezők ki vannak töltve. Zárójelben a create_table.sql script által létrehozott tábla megfelelő értékei szerepelnek. 

* f_table_schema: A projekttábla sémája a gisdata adatbázisban (public).
* f_table_name: A projekttábla, amelyből az adatokat kérdezzük le.
* f_id_column: A tábla egyedi azonosítóját tartalmazó mező neve (id).
* f_srid: A tábla vetületének epsg azonosítója (23700).
* f_geom_column: A táblában a geometriát tartalmazó oszlop neve (geom). 
* f_main_table: A tábla neve (hrsz vagy erdoreszlet).

A beillesztés lehetséges az insert_into.sql scriptek segítségével is, miután a projekttábla értéket lecseréltük arra a táblanévre, amelyben majd a lekérdezett adatokat keressük. Természetesen, amennyiben saját táblát hoztunk létre, vagy módosítottunk a create_table.sql scripten, értelemszerűen módosítani kell a megfelelő egyéb értékeket is.

A bejegyzés létrehozása mellett a táblában legalább SELECT jogosultsággal kell rendelkeznie az adott projekt projekt_admin felhasználójának, hogy a lekérdezések lefuthassanak.

### A gyorsított keresés előkészítése

Mivel nagy mennyiségű adatot kell használni, egy kiválasztott mező (pl. helyrajzi számok esetén a településnév) segítségével egy előszűrést hajt végre a modul, így csökkenti a keresési időt.
Ehhez a fenti mező tartalmának első betűjét egy új obm_first_letter_filter nevű oszlopba kell beírni a first_letter_filter.sql script segítségével.

## A modul beállítása

A modult a projekt könyvtárban az includes/modules/private mappában kell elhelyezni. Amennyiben szükséges, létre kell hozni a könyvtárat. A könyvtár jogosultságait célszerű úgy beállítani, hogy a www-data felhasználónak ne legyen írási jogosultsága. Ezzel elkerülhető, hogy az egyénileg létrehozott moduljaink felülíródjanak egy frissítés során.

A modult az OBM webes felületen az Adminisztráció/Modulok menüpontban tudjuk hozzáadni, ahol az alábbi értékeket kell beállítani.

* Modul név: box_custom
* Paraméterek: A modul fájlneve kiterjesztés nélkül.
* Engedélyezett: igen
* Hozzáférések: Igény szerint.

A megfelelő értékek megadása után a Módosít felirató gombra kattintva aktiválódik a modul a térképi nézeten.

## A modul használata

A használat egyszerű. A szűréshez használt mezőben található értéket begépelve a találatok megjelennek a mező alatt. Kattintással kiválasztjuk a megfelelőt. Az alsó mezőben megjelennek a szűrt értékek a lekérdezéshez használt mezőben, ahol egérkattintással választhatunk ki egy, valamint a SHIFT és/vagy a CTRL billentyűk segítségével egyszerre több elemet.
A térkép végigpörgeti, felvillantja a lekérdezett elemeket, a Kérdés gombra kattintva pedig megkapjuk a végső adatokat a projekt beállításainak megfelelő formá(k)ban.


