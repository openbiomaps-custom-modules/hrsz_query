/*Az erdo_query modul számára a minimálisan szükséges struktúrát létrehozó script.*/

CREATE TABLE "public"."erdoreszlet" (		/*sémanév.táblanév*/
	"id" SERIAL PRIMARY KEY, 		/*egyedi azonosító*/
	"telepules" character varying(20), 	/*településnév, szűrő oszlop neve*/
	"tag_reszlet" character varying(10)), 	/*Erdőtag, részletjel és alrészlet (pl. 12/A1), lekérdező oszlop*/
	"geom" geometry(Polygon,23700); 	/*geometria mező és vetület (HD72/EOV) EPSG azonosítója*/

