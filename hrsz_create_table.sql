/*A hrsz_query modul számára a minimálisan szükséges struktúrát létrehozó script.*/

CREATE TABLE "public"."hrsz" (			/*sémanév.táblanév*/
	"id" SERIAL PRIMARY KEY, 		/*egyedi azonosító*/
	"telepules" character varying(20), 	/*településnév, szűrő oszlop neve*/
	"hrsz_al" character varying(10)),	/*helyrajzi szám és alrészlet (pl. 0127/45a), lekérdező oszlop*/
	"geom" geometry(Polygon,23700);		/*geometria mező és vetület (HD72/EOV) EPSG azonosítója*/
