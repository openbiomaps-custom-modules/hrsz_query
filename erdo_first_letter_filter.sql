/*A keresés gyorsítását lehetővé tevő oszlop beállítása*/

	    /*sémanév.táblanév*/
ALTER TABLE public."erdoreszlet" ADD COLUMN obm_first_letter_filter character(1);
       /*sémanév.táblanév*/					   /*szűrő oszlop*/
UPDATE public.erdoreszlet SET obm_first_letter_filter=lower(substring(telepules from 1 for 1));
			   /*szűrő oszlop*/ /*szűrő oszlop*/ /*sémanév.táblanév*/				    /*szűrő oszlop*/		    /*szűrő oszlop*/
EXPLAIN ANALYZE SELECT DISTINCT "telepules" "telepules" FROM public."erdoreszlet" WHERE obm_first_letter_filter='g' AND telepules ILIKE 'új%' ORDER BY telepules LIMIT 20;
